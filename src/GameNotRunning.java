
public class GameNotRunning extends GameState {
	public GameNotRunning(GameContext gameContext) {
		super();
	}

	@Override
	public
	String makeMove(String input, String name) {
		return "No active game active, you cannot make a move at this moment";
	}

	@Override
	public String gameStatus() {
	    return ("No active game running, join a team to start!\n" +  GameContext.getGameArea());
	}

}