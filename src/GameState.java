
public abstract class GameState {
	GameContext gameContext;	
	abstract public String gameStatus();
	public abstract String makeMove(final String input,final String name);	
}