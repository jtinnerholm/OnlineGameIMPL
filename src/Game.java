import java.io.Console;
import java.lang.String;
import java.util.ArrayDeque;
import java.util.LinkedList; 
import java.util.ArrayList;
import java.util.List;

class Game{
	private int row = 9, col = 9; 
	private char[][] gameArea = new char[row][col];
	private LinkedList<String> validMoves = new LinkedList<String>();
	private String[] Teams = new String[2];
	private LinkedList<String> TeamX  = new LinkedList <String>();
	private LinkedList<String> TeamO  = new LinkedList <String>();
	private boolean order;
	public boolean gameRunning;

	public Game(){
		String tmp = "";
		char c = 'A';	    
		for(int i = 0 ; i < 9; ++i){	    
			for(int j = 0; j < 9; ++j){
				gameArea[i][j] = '-';
				tmp = Character.toString(c) + Integer.toString(j+1);
				validMoves.add(tmp);
			}
			++c; //move to next character
		}
		gameRunning = false;
		Teams[0] = "TeamX";
		Teams[1] = "TeamO";
		order = true;
	}

	public String leaveGame(final String name){

		if(TeamX.contains(name)){
			TeamX.remove(name);
			return "You left TeamX!";
		}
		else if(TeamO.contains(name)){
			TeamO.remove(name);
			return "You left TeamO";
		}
		return "You are not playing the game and thus you cannot leave\n";
	}

	public String joinGame(final String requestedTeam,final String name){
		if(TeamX.contains(name) || TeamO.contains(name)){
			return ("You are already in a team\n");
		}

		if(TeamX.size() == 0 && TeamO.size() == 0){
			TeamX.add(name);
			gameRunning = true;
			return ("There were not active player in any team, Welcome " + name + " to TeamX");
		}
		else if(TeamO.size() == 0 ){
			TeamO.add(name);
			gameRunning = true;
			return ("There where no active player in TeamO!, welcome to TeamO " + name );
		}	
		if(requestedTeam.equals("TeamX")){
			TeamX.add(name);
			gameRunning = true;
			return ("Welcome " + name + " to TeamX");
		}
		else if(requestedTeam.equals("TeamO")){
			TeamO.add(name);
			gameRunning = true;
			return ("Welcome " + name + " to TeamO");
		}
		else{
			return ("The team that you requested to join does not exist");
		}
	}

	private String getGameArea(){
		String ret = " ";
		for(int i = 0; i < 9; ++i){
			ret += (" " + Integer.toString(i+1));
		}
		ret += "\n";

		for(int i = 0 ; i < row; ++i){	    
			char c = (char)(i + 65);
			ret += Character.toString(c);
			for(int j = 0; j < col; ++j){
				ret += (" "+(gameArea[i][j])); 		    
			}
			ret += "\n";
		}
		return ret;
	}

	public String gameStatus(){

		int winningTeam = 0;
		String ret = "";
		if(gameRunning == false){	    
			return ("No active game running, join a team to start!\n" +  getGameArea());
		}

		if(Teams[0].equals("TeamX") ){
			ret = TeamX.peek();
		}
		else{
			ret = TeamO.peek();
		}
		if(ret == null){
			return "a Team lacks members leave to reset the game";
		}
		winningTeam  = won();

		if(winningTeam == 1){
			return ("Team X is the winner!\n" + getGameArea());

		}
		else if(winningTeam == 2){
			return ("Team O is the winner!\n" + getGameArea());
		}

		else if(isDraw()){	   
			return ("Game ended in draw! Good Job!\n" + getGameArea());
		}
		else if(order){
			return "Current situation\n" + getGameArea() + "It is " + ret + " turn \n";
		}
		else{
			return "Current situation\n" + getGameArea() + "It is " + Teams[0]  + " turn \n";
		}
	}

	private boolean isDraw(){	
		for(int i = 0 ; i < row; ++i){	    
			for(int j = 0; j < col; ++j){
				if(gameArea[i][j] == '-'){
					return false;
				}
			}
		}
		return true;
	}

	private int won(){
		if(checkHorizontal('X','O') || 
				checkVertical('X','O') || 
				checkDiagonalFromLeft('X') ||
				checkDiagonalFromRight('X')){
			return 1;
		}
		else if(checkHorizontal('O','X') || 
				checkVertical('O','X') || 
				checkDiagonalFromLeft('O') ||
				checkDiagonalFromRight('O')){
			return 2;
		}
		return 3;
	}

	/* x is the current team, y the other*/
	private boolean checkHorizontal(final char x, final char y){
		int nrX = 0;

		for(int i = 0 ; i < row; ++i){	    
			for(int j = 0; j < col; ++j){
				if(gameArea[i][j] == x) {
					++nrX;
					if(nrX == 5){
						return true;
					}
				}
				else if(gameArea[i][j] == y || gameArea[i][j] == '-'){
					nrX = 0;
				}
			}
		}
		return false;
	}

	private boolean checkVertical(final char x, final char y){
		int nrX = 0;
		for(int j = 0 ; j < col; ++j){	    
			for(int i = 0; i < row; ++i){
				if(gameArea[i][j] == x) {
					++nrX;
					if(nrX == 5){
						return true;
					}
				}
				else if(gameArea[i][j] == y || gameArea[i][j] == '-'){
					nrX = 0;
				}
			}
		}
		return false;
	}

	private boolean checkDiagonalFromLeft(final char marker){
		int nrMarkers = 0;
		int increase_row = 0;

		for(int check = 0; check < 21; ++check) {
			System.out.println(check);
			for(int i = increase_row, j = 0; i < (5 + increase_row); ++i,++j){		
				if(gameArea[i][j+(check%5)] == marker ){
					++nrMarkers;
					if(nrMarkers == 5){
						return true;
					}
				}
				else {
					nrMarkers = 0;
				}
			}
			if(check % 5 == 0){
				++increase_row;
				nrMarkers = 0;
			}	 
		}

		return false;
	}

	private boolean checkDiagonalFromRight(final char marker){
		int nrMarkers = 0;
		int increase_row = 0;

		for(int check = 0; check < 21; ++check) {
			System.out.println(check);
			for(int i = increase_row, j = 8; i < (5 + increase_row); ++i,--j){		
				if(gameArea[i][j-(check%5)] == marker ){
					++nrMarkers;
					if(nrMarkers == 5){
						return true;
					}
				}
				else {
					nrMarkers = 0;
				}
			}
			if(check % 5 == 0){
				++increase_row;
				nrMarkers = 0;
			}	 
		}

		return false;
	}

	public String makeMove(final String input,final String name){
		String playerTeam = "";
		String activePlayer = name;

		if(TeamO.contains(name)){
			playerTeam = "TeamO";
		}
		else if(TeamX.contains(name)){
			playerTeam = "TeamX";
		}
		else{
			return ("You have to enter a valid team to particapte in the game!");
		}

		if(!moveIsvalid(input)){
			return("You have to enter a valid move to play, try again!");
		}

		if(Teams[0].equals(playerTeam)){
			if(Teams[0].equals("TeamX")){
				if(TeamX.peek().equals(name)|| !order){
					placeMarker(input,playerTeam);
					if(order){ //If we play regular mode
						TeamX.poll();
						TeamX.add(activePlayer);
					}
					swapTeams();
					return (name + " placed marker: " + input);
				}
			}
			else{ //In that case it is TeamO:s turn	
				if(TeamO.peek().equals(name) || !order){
					placeMarker(input,playerTeam);
					if(order){ //If we play regular mode
						TeamO.poll();
						TeamO.add(activePlayer);
					}
					swapTeams();
					return (name + " placed marker: " + input);
				}
			}	    
		}
		return ("You have failed to make a move");
	}

	/*Checks if it is a valid command and if so places the marker 
      notice the hack below */
	private void placeMarker(final String move,final String team){

		int letter = (int)move.charAt(0) -65 ;
		int number = Character.getNumericValue(move.charAt(1))-1;
		System.out.println(number);
		if(team.equals("TeamX"))
			gameArea[letter][number] = 'X';
		else{
			gameArea[letter][number] = 'O';
		}
	}
	private void swapTeams(){
		String temp = Teams[1]; 
		Teams[1] = Teams[0];
		Teams[0] = temp;
	}

	public boolean getPlayers(final String name){
		List<String> players = new ArrayList<String>(TeamX);
		players.addAll(TeamO);

		if(players.contains(name)){
			return true;
		}
		return false;
	}

	public void resetGame(){
		for(int i = 0 ; i < 9; ++i){	    
			for(int j = 0; j < 9; ++j){
				gameArea[i][j] = '-';
			}
		}
		gameRunning = false;
		Teams[0] = "TeamX";
		Teams[1] = "TeamO";
		TeamX.clear();
		TeamO.clear();	
	}

	private boolean moveIsvalid(final String move){
		int letter = (int)move.charAt(0) -65 ;
		int number = Character.getNumericValue(move.charAt(1))-1;

		if(!validMoves.contains(move) ||
				gameArea[letter][number] == 'X' || 
				gameArea[letter][number] == 'O'){
			return false;
		}
		return true;
	}

	public String ChangeMode(boolean bool){
		if(!gameRunning){
			order = bool;
		}

		return "A game is already running";
	}

}
