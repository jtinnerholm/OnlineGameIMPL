import ChatApp.*;          // The package containing our stubs
import org.omg.CosNaming.*; // HelloClient will use the naming service.
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;     // All CORBA applications need these classes.
import org.omg.PortableServer.*;   
import java.io.Console;
import java.lang.String;
import java.util.Scanner;
 
class ChatCallbackImpl extends ChatCallbackPOA
{
    private ORB orb;

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    public void callback(String notification)
    {
        System.out.println(notification);
    }
}

public class ChatClient
{
    static Chat chatImpl;
    
    public static void main(String args[])
    {
	try {
	    // create and initialize the ORB
	    ORB orb = ORB.init(args, null);

	    // create servant (impl) and register it with the ORB
	    ChatCallbackImpl chatCallbackImpl = new ChatCallbackImpl();
	    chatCallbackImpl.setORB(orb);

	    // get reference to RootPOA and activate the POAManager
	    POA rootpoa = 
		POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();
	    
	    // get the root naming context 
	    org.omg.CORBA.Object objRef = 
		orb.resolve_initial_references("NameService");
	    NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
	    
	    // resolve the object reference in naming
	    String name = "Chat";
	    chatImpl = ChatHelper.narrow(ncRef.resolve_str(name));
	    
	    // obtain callback reference for registration w/ server
	    org.omg.CORBA.Object ref = 
		rootpoa.servant_to_reference(chatCallbackImpl);
	    ChatCallback cref = ChatCallbackHelper.narrow(ref);
	    
	    /*Todo divide the code into more clear function 
	      & fix so all the cases takes args except post which takes a entire string!
	      All functions except list and post also takes an argument of type string remember 
	      to fix this*/
	    /* Application code goes below */
	    
	    System.out.println("Welcome to the lobby!");
	    
	    boolean activeChat = true; //User joined the chat, the chat is active
	    Scanner scanner =  new Scanner(System.in);
	    String msg = "";
	    String argumentString =""; 
	    String currentChatter = "nil"; //Default name is nil
	    String argumentArray[] = {""};
	    
	    while(activeChat){ 
		msg = scanner.nextLine();
		if(msg.isEmpty()){
		    System.out.println("Client Error: Invalid input");
		    msg = "nil";
		}
		argumentArray = msg.split(" ");
		argumentString = createArgumentString(argumentArray);
		switch (argumentArray[0]){
		case "leave":{
		    if(argumentArray.length == 1){
			if(!currentChatter.equals("nil")){  
				currentChatter = chatImpl.leave(cref,argumentString);
				break;
			}
			else{
			    System.out.println("Client: error no active session");
			    break;
			}
		    }
		}
		case "join":{
		    if((currentChatter.equals("nil") && argumentArray.length > 1 )){
			currentChatter = chatImpl.join(cref,argumentString);
			break;
		    }
		    else{
			System.out.print("Client error: Error you are already logged in as: ");
			System.out.print (currentChatter + " Please leave and join again\n");
			break;
		    }
		}
		case "post": {
		    if(!currentChatter.equals("nil")){
			chatImpl.post(cref, argumentString);
			break;
		    }
		    else {
			System.out.print("Client error: Error");
			System.out.print("you must have a valid username before you post\n");				    
			break;
		    }
		}
		case "list":{
		    chatImpl.list(cref);
		    break;
		}
		case "exit":{ 
		    currentChatter = chatImpl.leave(cref,currentChatter);
		    activeChat = false;
		    break;
		}
		case "game":{
		    chatImpl.gameInterface(cref, argumentString);
		    break;
		}

		default: {
		    System.out.println("Client error: unknown command");
		}
		}
	    }	    
	} //End of loop
	catch(Exception e){
	    System.out.println("ERROR : " + e);
	    e.printStackTrace(System.out);
	}
    }
    
    /* Simple method, merges the rest of the parameters 
       in the argumentArray into a argumentString
     */
    private static String createArgumentString(String argumentArray[]){
	String argumentString = "";
	if(argumentArray.length == 0){
	    System.out.println("Client error: invalid command");
	}
	for(int i = 1; i < argumentArray.length; ++i){
	    argumentString = argumentString + argumentArray[i] + " ";
	}
	if(argumentArray.length > 1){
	    return argumentString.substring(0,argumentString.length()-1); //Removes redundant space.
	}
	return "";
    }
}
