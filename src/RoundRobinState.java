
public class RoundRobinState extends GameState {
	RoundRobinState(GameContext gameContext){
		this.gameContext = gameContext;
	}
	
	@Override
	public
	String makeMove(final String input, final String name) {
		String playerTeam = "";
		String activePlayer = name;
		
		if(gameContext.getTeamO().contains(name)){
			playerTeam = "TeamO";
		}
		else if(gameContext.getTeamX().contains(name)){
			playerTeam = "TeamX";
		}
		else{
			return ("You have to enter a valid team to particapte in the game!");
		}

		if(!gameContext.moveIsvalid(input)){
			return("You have to enter a valid move to play, try again!");
		}

		if(gameContext.getTeams()[0].equals(playerTeam)){
			if(gameContext.getTeams()[0].equals("TeamX")){
				if(gameContext.getTeamX().peek().equals(name)){
					gameContext.placeMarker(input,playerTeam);
					gameContext.getTeamX().poll();
					gameContext.getTeamX().add(activePlayer);
					gameContext.swapTeams();
					return (name + " placed marker: " + input);
				}
			}
			else{ //In that case it is TeamO:s turn	
				if(gameContext.getTeamO().peek().equals(name)){
					gameContext.placeMarker(input,playerTeam);
					gameContext.getTeamO().poll();
					gameContext.getTeamO().add(activePlayer);
					gameContext.swapTeams();
					return (name + " placed marker: " + input);
				}
			}	    
		}
		return ("You have failed to make a move");
	}

	@Override
	public String gameStatus() {
		String ret = "";
	
		//Since this is round robin we need to get the current player
		System.out.println("RoundRobinGameStatis");
		System.out.println(gameContext.getTeams()[0]);
		
		if(gameContext.getTeams()[0].equals("TeamX") ){
			System.out.println(gameContext.getTeamX().peek());
			ret = gameContext.getTeamX().peek();
		}
		else{
			ret = gameContext.getTeamO().peek();
		}
		if(ret == null){
			return "a Team lacks members leave to reset the game";
		}

		if(gameContext.won() == 1){
			gameContext.stateTransistion(gameContext.gameNotrunningState);
			return ("Team X is the winner!\n" + GameContext.getGameArea());
		}
		else if(gameContext.won() == 2){
			gameContext.stateTransistion(gameContext.gameNotrunningState);
			return ("Team O is the winner!\n" + GameContext.getGameArea());
		}
		else if(gameContext.isDraw()){	   
			return ("Game ended in draw! Good Job!\n" + GameContext.getGameArea());
		}
		else {
			return "Current situation\n" + GameContext.getGameArea() + "It is " + ret + " turn \n";
		}
		
	}
}
