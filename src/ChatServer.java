import ChatApp.*;          // The package containing our stubs. 
import org.omg.CosNaming.*; // HelloServer will use the naming service. 
import org.omg.CosNaming.NamingContextPackage.*; // ..for exceptions. 
import org.omg.CORBA.*;     // All CORBA applications need these classes. 
import org.omg.PortableServer.*;   
import org.omg.PortableServer.POA;
import java.util.HashMap;
import java.util.List; 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays.*;

class ChatImpl extends ChatPOA{
	private ORB orb;
	private HashMap<ChatCallback,String> activeUsers = new HashMap<ChatCallback,String>();   
	private GameContext gameContext= new GameContext();

	public void setORB(ORB orb_val) {
		orb = orb_val;
	}

	public String post(ChatCallback callobj, String msg){
		for (ChatCallback key : activeUsers.keySet()) {
			if(!key.equals(callobj)){
				key.callback(activeUsers.get(callobj) + " said: " + msg );
			}
		}
		return msg;
	}

	public String leave (ChatCallback callobj, String msg){
		if(activeUsers.get(callobj) != null){
			callobj.callback("Goodbye " + activeUsers.get(callobj));
			for (ChatCallback key : activeUsers.keySet()) {
				if(!key.equals(callobj)){
					key.callback(activeUsers.get(callobj) + " left: " );
				}
			}
			gameContext.leaveGame(activeUsers.get(callobj));
			activeUsers.remove(callobj);	    
			return "nil";
		}
		else{
			System.out.println("Server Error: could not remove " + msg);
			return msg;
		}
	}

	public String join (ChatCallback callobj, String person){
		if(activeUsers.isEmpty()){
			activeUsers.put(callobj,person);
			callobj.callback("Welcome: " + person);
			return person;
		}
		else if(activeUsers.containsValue(person)){	    
			callobj.callback("Server Error: user " + person + " is already an active chatter");
			return "nil";
		}
		else{
			activeUsers.put(callobj,person);
			callobj.callback("Welcome: " + person);
			for (ChatCallback key : activeUsers.keySet()) {
				if(!key.equals(callobj)){
					key.callback(activeUsers.get(callobj) + " joined " );
				}
			}
			return person;
		}   
	}
	public boolean list (ChatCallback callobj){
		callobj.callback("List of active users");
		if(activeUsers.isEmpty()){
			callobj.callback("Server error: Error no active users");
			return false;
		}
		else{
			List<String> users = new ArrayList<String>();
			for (ChatCallback key : activeUsers.keySet())
				users.add(activeUsers.get(key));

			Collections.sort(users);

			for(String user : users)
				callobj.callback(user);

			return true;
		}
	}
	public void gameInterface(ChatCallback callobj, String arguments){
		String argumentArray[] = arguments.split(" ");

		if(activeUsers.isEmpty() || (activeUsers.get(callobj) == null)){
			callobj.callback("Server error: Error no active users");
			getCurrentSituation();
			return;
		}

		if(arguments.equals("")){
			callobj.callback("If you would like to play our fantastic game, please enter game followed by a command");
			getCurrentSituation();
			return ;
		}

		switch(argumentArray[0]){	
		case "join" :{	    
			if(argumentArray.length == 2){
				callobj.callback(gameContext.joinGame(argumentArray[1],activeUsers.get(callobj)));
				for (ChatCallback key : activeUsers.keySet()) {
					if(!key.equals(callobj)){
						key.callback(activeUsers.get(callobj) + " joined the game " );
					}
					return;
				}
			}
			callobj.callback("Server error: You can only join TeamX or TeamO, please enter a valid command.\n");
			getCurrentSituation();
			return;
		}

		case "leave" : {
			callobj.callback(gameContext.leaveGame(activeUsers.get(callobj)));
			getCurrentSituation();
			return;
		}

		case "move" : {
			if(argumentArray.length == 2){
				String tmp = gameContext.makeMove(argumentArray[1],activeUsers.get(callobj));
				if(tmp.contains("failed") || tmp.contains("valid")){
					callobj.callback(tmp);
					return;
				}

				for (ChatCallback key : activeUsers.keySet()) {
					if((gameContext.getPlayers(activeUsers.get(key)) && !key.equals(callobj))){
						key.callback(tmp);
					}		    
				}			
				getCurrentSituation();
				return;
			}
			else{
				callobj.callback("Server Error:Please enter a valid command.\n");
				getCurrentSituation();
				return;
			}
		}

		case "status":{
			if(argumentArray.length == 1){
				callobj.callback(gameContext.gameStatus());
				return;
			}
			else{
				callobj.callback("Server Error:Please enter a valid command.\n");
				getCurrentSituation();
				return;
			}
		}
		case "mode":{
			if(argumentArray.length == 1){
				gameContext.modeChange();
				callobj.callback("Mode Changed.\n");
				return;
			}
			else{
				callobj.callback("Server Error:Please enter a valid command.\n");
				return;
			}   
		}
		default: {		
			callobj.callback("Hello, you enter a invalid command,but if you want to play the game. Please enter Game join and chose a valid team!\n");
			getCurrentSituation();
			return;
		}      
		}
	}
	private void getCurrentSituation(){
		String tmp = "";
		tmp = gameContext.gameStatus();
		for (ChatCallback key : activeUsers.keySet()) {
			if(gameContext.getPlayers(activeUsers.get(key))){	
				key.callback(tmp);
			}
		}
		if(tmp.contains("winner")){
			gameContext.resetGame();
		}
	}
}     

public class ChatServer{
	public static void main(String args[]){
		try { 
			// create and initialize the ORB
			ORB orb = ORB.init(args, null); 

			// create servant (impl) and register it with the ORB
			ChatImpl chatImpl = new ChatImpl();
			chatImpl.setORB(orb); 

			// get reference to rootpoa & activate the POAManager
			POA rootpoa = 
					POAHelper.narrow(orb.resolve_initial_references("RootPOA"));  
			rootpoa.the_POAManager().activate(); 

			// get the root naming context
			org.omg.CORBA.Object objRef = 
					orb.resolve_initial_references("NameService");
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			// obtain object reference from the servant (impl)
			org.omg.CORBA.Object ref = 
					rootpoa.servant_to_reference(chatImpl);
			Chat cref = ChatHelper.narrow(ref);

			// bind the object reference in naming
			String name = "Chat";
			NameComponent path[] = ncRef.to_name(name);
			ncRef.rebind(path, cref);

			// Application code goes below
			System.out.println("ChatServer ready and waiting ...");

			// wait for invocations from clients
			orb.run();
		}

		catch(Exception e) {
			System.err.println("ERROR : " + e);
			e.printStackTrace(System.out);
		}

		System.out.println("ChatServer Exiting ...");
	}
}

