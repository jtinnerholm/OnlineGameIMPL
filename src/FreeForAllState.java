
public class FreeForAllState extends GameState {

	public FreeForAllState(GameContext gameContext) {
		this.gameContext = gameContext; 
	}

	@Override
	public String makeMove(final String input, final String name) {
		String playerTeam = "";

		if(gameContext.getTeamO().contains(name)){
			playerTeam = "TeamO";
		}
		else if(gameContext.getTeamX().contains(name)){
			playerTeam = "TeamX";
		}
		else{
			return ("You have to enter a valid team to play the game!");
		}

		if(!gameContext.moveIsvalid(input)){
			return("You have to enter a valid move to play, try again!");
		}

		if(gameContext.getTeams()[0].equals(playerTeam)){
			if(gameContext.getTeams()[0].equals("TeamX")){
				gameContext.placeMarker(input,playerTeam);
				gameContext.swapTeams();
				return (name + " placed marker: " + input);
			}
			else{ //In that case it is TeamO:s turn	
				gameContext.swapTeams();
				return (name + " placed marker: " + input);
			}
		}
		return "You failed to make a move";	    
	}

	@Override
	public String gameStatus() {
		// TODO Auto-generated method stub
    	if(gameContext.getTeamO().peek() == null || gameContext.getTeamO().peek()== null){
    	    return "a Team lacks members leave to reset the game";
    	}
    	
    	if(gameContext.won() == 1){
    	    return ("Team X is the winner!\n" + GameContext.getGameArea());
    	    
    	}
    	else if(gameContext.won() == 2){
    		gameContext.stateTransistion(gameContext.getGameNotRunningState());
    	    return ("Team O is the winner!\n" + GameContext.getGameArea());
    	}
    	//Turn handling. 
    	else if(gameContext.isDraw()){
    			gameContext.stateTransistion(gameContext.getGameNotRunningState());
          	    return ("Game ended in draw! Good Job!\n" + GameContext.getGameArea());
    	}
    	
    	    return "Current situation\n" + GameContext.getGameArea() + "It is " + gameContext.getTeams()[0]  + " turn \n";
	}
}
